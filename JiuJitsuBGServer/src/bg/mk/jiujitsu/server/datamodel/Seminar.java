package bg.mk.jiujitsu.server.datamodel;

import java.util.Date;
import java.util.List;

import javax.jdo.annotations.IdentityType;
import javax.jdo.annotations.PersistenceCapable;
import javax.jdo.annotations.Persistent;

@PersistenceCapable(identityType = IdentityType.APPLICATION)
public class Seminar extends Event {

	/**
	 * This holder keeps keys of coaches as strings
	 */
	@Persistent
	private GaeListHolder coachKeys;
	
	public Seminar(String title, Date date, Location location, String info,
			List<String> coaches) {
		super(title, date, location, info);
		this.coachKeys = new GaeListHolder(coaches);
	}

	public Seminar(String title, Date date, Location location,
			List<String> imageUrls, List<String> videoIds, String info) {
		super(title, date, location, imageUrls, videoIds, info);
		coachKeys = new GaeListHolder();
	}

	public Seminar(String title, Date date, Location location, String info) {
		super(title, date, location, info);
		coachKeys = new GaeListHolder();
	}

	public List<String> getCoachKeys() {
		if(coachKeys == null) {
			coachKeys = new GaeListHolder();
		}
		return coachKeys.getList();
	}

	public void setCoachKeys(List<String> coachKeys) {
		if(this.coachKeys != null) {
			this.coachKeys.setList(coachKeys);
		} else {
			this.coachKeys = new GaeListHolder(coachKeys);
		}
	}

}
