package bg.mk.jiujitsu.server.datamodel;

import java.util.Date;
import java.util.List;

import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import javax.jdo.annotations.IdentityType;
import javax.jdo.annotations.NotPersistent;
import javax.jdo.annotations.PersistenceCapable;
import javax.jdo.annotations.Persistent;

import bg.mk.jiujitsu.server.datastore.PMF;

@PersistenceCapable(identityType = IdentityType.APPLICATION)
public class Competition extends Event {

	@Persistent
	private GaeListHolder competitorKeys;
	
	@NotPersistent
	private List<Competitor> competitors;
	
	@Persistent
	private GaeListHolder judgeKeys;
	
	public Competition(String title, Date date, Location location,
			List<String> imageUrls, List<String> videoIds, String info) {
		super(title, date, location, imageUrls, videoIds, info);
		this.competitorKeys = new GaeListHolder();
		this.judgeKeys = new GaeListHolder();
	}

	public Competition(String title, Date date, Location location, String info) {
		this(title, date, location, info, null, null);
	}

	public Competition(String title, Date date, Location location, String info,
			List<String> competitors, List<String> judges) {
		super(title, date, location, info);
		this.competitorKeys = new GaeListHolder(competitors);
		this.judgeKeys = new GaeListHolder(judges);
	}

	public List<String> getCompetitorKeys() {
		if(competitorKeys == null) {
			competitorKeys = new GaeListHolder();
		}
		return competitorKeys.getList();
	}

	public void setCompetitorKeys(List<String> competitorKeys) {
		if(this.competitorKeys != null) {
			this.competitorKeys.setList(competitorKeys);
		} else {
			this.competitorKeys = new GaeListHolder(competitorKeys);
		}
	}

	public List<String> getJudgeKeys() {
		if(judgeKeys == null) {
			judgeKeys = new GaeListHolder();
		}
		return judgeKeys.getList();
	}

	public void setJudgeKeys(List<String> judgeKeys) {
		if(this.judgeKeys != null) {
			this.judgeKeys.setList(judgeKeys);
		} else {
			this.judgeKeys = new GaeListHolder(judgeKeys);
		}
	}

	public List<Competitor> getCompetitors() {
		if(competitors == null && !getCompetitorKeys().isEmpty()) {
			loadCompetitors();
		}
		return competitors;
	}
	
	public void setCompetitors(List<Competitor> competitors) {
		this.competitors = competitors;
	}

	@SuppressWarnings("unchecked")
	private void loadCompetitors() {
		PersistenceManager pm = PMF.get().getPersistenceManager();
		Query query = pm.newQuery(Competitor.class);
		
		List<String> keys = getCompetitorKeys();
		if (keys != null && !keys.isEmpty()) {
			query.setFilter(":keys.contains(key)");
			competitors = (List<Competitor>) pm.newQuery(query).execute(keys);
		}
	}
	
}
