package bg.mk.jiujitsu.server.datamodel;

import javax.jdo.annotations.IdentityType;
import javax.jdo.annotations.PersistenceCapable;
import javax.jdo.annotations.Persistent;

@PersistenceCapable(identityType = IdentityType.APPLICATION)
public class Participant extends Person {

	@Persistent
	private int weight;

	public Participant(String firstName, String lastName) {
		super(firstName, lastName);
		this.weight = -1;
	}

	public Participant(String firstName, String lastName, int weight) {
		super(firstName, lastName);
		this.weight = weight;
	}
	
	public Participant(String firstName, String lastName, int weight, int age) {
		super(firstName, lastName, age);
		this.weight = weight;
	}

	public int getWeight() {
		return weight;
	}

	public void setWeight(int weight) {
		this.weight = weight;
	}
	

}
