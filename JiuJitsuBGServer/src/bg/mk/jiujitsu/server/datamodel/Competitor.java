package bg.mk.jiujitsu.server.datamodel;

import javax.jdo.annotations.IdentityType;
import javax.jdo.annotations.PersistenceCapable;
import javax.jdo.annotations.Persistent;

@PersistenceCapable(identityType = IdentityType.APPLICATION)
public class Competitor extends Person {

	@Persistent
	private int weight;
	
	@Persistent
	private int category;
	
	@Persistent
	private String reward;
	
	public Competitor(String firstName, String lastName) {
		super(firstName, lastName);
	}
	
	public Competitor(String firstName, String lastName, int age) {
		super(firstName, lastName, age);
	}

	public Competitor(String firstName, String lastName, int age, int weight,
			int category, String reward) {
		super(firstName, lastName, age);
		this.weight = weight;
		this.category = category;
		this.reward = reward;
	}

	public int getWeight() {
		return weight;
	}

	public void setWeight(int weight) {
		this.weight = weight;
	}

	public int getCategory() {
		return category;
	}

	public void setCategory(int category) {
		this.category = category;
	}

	public String getReward() {
		return reward;
	}

	public void setReward(String reward) {
		this.reward = reward;
	}

}
