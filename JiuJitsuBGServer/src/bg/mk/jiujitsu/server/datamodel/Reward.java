package bg.mk.jiujitsu.server.datamodel;

import java.util.Date;

import javax.jdo.annotations.IdGeneratorStrategy;
import javax.jdo.annotations.IdentityType;
import javax.jdo.annotations.PersistenceCapable;
import javax.jdo.annotations.Persistent;
import javax.jdo.annotations.PrimaryKey;

import com.google.appengine.api.datastore.Key;

@PersistenceCapable(identityType = IdentityType.APPLICATION)
public class Reward {
	
	@PrimaryKey
	@Persistent(valueStrategy = IdGeneratorStrategy.IDENTITY)
	private Key key;
	
	@Persistent
	private String title;
	
	@Persistent
	private Date date;

	@Persistent
	private Key competitionKey;

	public Reward(String title, Date date) {
		this.title = title;
		this.date = date;
	}
	
	public Reward(String title, Date date, Key competitionKey) {
		this(title, date);
		this.competitionKey = competitionKey;
	}

	public Key getKey() {
		return key;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public Key getCompetitionKey() {
		return competitionKey;
	}

	public void setCompetitionKey(Key key) {
		this.competitionKey = key;
	} 

}
