package bg.mk.jiujitsu.server.datamodel;

import java.util.Date;

import javax.jdo.annotations.IdentityType;
import javax.jdo.annotations.PersistenceCapable;
import javax.jdo.annotations.Persistent;

@PersistenceCapable(identityType = IdentityType.APPLICATION)
public class Member extends Person {

	@Persistent
	private Date startDate;
	
	@Persistent
	private int weight;
	
	@Persistent
	private int degree;
	
	@Persistent
	private String info;

	public Member(String firstName, String lastName, Date startDate,
			int weight, int degree, String info) {
		super(firstName, lastName);
		this.startDate = startDate;
		this.weight = weight;
		this.degree = degree;
		this.info = info;
	}

	public Member(String firstName, String lastName) {
		super(firstName, lastName);
	}

	public Member(String firstName, String lastName, int age) {
		super(firstName, lastName, age);
	}

	public Member(String firstName, String lastName, int age, String info) {
		super(firstName, lastName, age);
		this.info = info;
	}

	public String getInfo() {
		return info;
	}

	public void setInfo(String info) {
		this.info = info;
	}

	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	public int getWeight() {
		return weight;
	}

	public void setWeight(int weight) {
		this.weight = weight;
	}

	public int getDegree() {
		return degree;
	}

	public void setDegree(int degree) {
		this.degree = degree;
	}

}
