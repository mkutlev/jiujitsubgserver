package bg.mk.jiujitsu.server.datamodel;

import javax.jdo.annotations.IdentityType;
import javax.jdo.annotations.PersistenceCapable;

@PersistenceCapable(identityType = IdentityType.APPLICATION)
public class Judge extends Person {

	public Judge(String firstName, String lastName) {
		super(firstName, lastName);
	}
	
	public Judge(String firstName, String lastName, int age) {
		super(firstName, lastName, age);
	}

}
