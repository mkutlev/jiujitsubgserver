package bg.mk.jiujitsu.server.datamodel;

import java.util.Date;
import java.util.List;

import javax.jdo.annotations.IdentityType;
import javax.jdo.annotations.PersistenceCapable;
import javax.jdo.annotations.Persistent;

@PersistenceCapable(identityType = IdentityType.APPLICATION)
public class Coach extends Person {

	@Persistent
	private Date startCoachingDate;
	
	@Persistent
	private int degree; 
	
	@Persistent
	private String info;
	
	/**
	 * Holder of list of urls pointing to images.
	 */
	@Persistent
	private GaeListHolder imageUrls;
	
	/**
	 * Holder of list of ids for YouTube videos
	 */
	@Persistent
	private GaeListHolder videoIds;
	
	public Coach(String firstName, String lastName, int age) {
		this(firstName, lastName, null, 0, null);
		this.setAge(age);
	}

	public Coach(String firstName, String lastName) {
		this(firstName, lastName, 0);
	}

	public Coach(String firstName, String lastName, Date startCoachingDate,
			int degree, String info) {
		super(firstName, lastName);
		this.startCoachingDate = startCoachingDate;
		this.degree = degree;
		this.info = info;
		this.imageUrls = new GaeListHolder();
		this.videoIds = new GaeListHolder();
	}

	public Coach(String firstName, String lastName, Date startCoachingDate,
			int degree, String info, List<String> imageUrls,
			List<String> videoIds) {
		this(firstName, lastName, startCoachingDate, degree, info);
		this.imageUrls = new GaeListHolder(imageUrls);
		this.videoIds = new GaeListHolder(videoIds);
	}

	public Date getStartCoachingDate() {
		return startCoachingDate;
	}

	public void setStartCoachingDate(Date startCoachingDate) {
		this.startCoachingDate = startCoachingDate;
	}

	public int getDegree() {
		return degree;
	}

	public void setDegree(int degree) {
		this.degree = degree;
	}

	public String getInfo() {
		return info;
	}

	public void setInfo(String info) {
		this.info = info;
	}

	/**
	 * Returns a list of urls for images
	 * 
	 * @return list of strings
	 */
	public List<String> getImageUrls() {
		if (imageUrls == null) {
			imageUrls = new GaeListHolder();
			}
		return imageUrls.getList();
	}

	public void setImageUrls(List<String> imageUrls) {
		if(this.imageUrls != null) {
			this.imageUrls.setList(imageUrls);
		} else {
			this.imageUrls = new GaeListHolder(imageUrls);
		}
	}

	/**
	 * Returns a list of YouTube video ids
	 * 
	 * @return list of ids
	 */
	public List<String> getVideoIds() {
		if (videoIds == null) {
			videoIds = new GaeListHolder();
		}
		return videoIds.getList();
	}

	public void setVideoIds(List<String> videoIds) {
		if(this.videoIds != null) {
			this.videoIds.setList(videoIds);
		} else {
			this.videoIds = new GaeListHolder(videoIds);
		}
	}

}
