package bg.mk.jiujitsu.server.datamodel;

import java.util.Date;
import java.util.List;

import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import javax.jdo.annotations.IdentityType;
import javax.jdo.annotations.NotPersistent;
import javax.jdo.annotations.PersistenceCapable;
import javax.jdo.annotations.Persistent;

import bg.mk.jiujitsu.server.datastore.PMF;

@PersistenceCapable(identityType = IdentityType.APPLICATION)
public class Demonstration extends Event {

	@Persistent
	private GaeListHolder participantKeys;
	
	@NotPersistent
	private List<Participant> participants;
	
	public Demonstration(String title, Date date, Location location,
			List<String> imageUrls, List<String> videoIds, String info) {
		super(title, date, location, imageUrls, videoIds, info);
		participantKeys = new GaeListHolder();
	}

	public Demonstration(String title, Date date, Location location, String info) {
		super(title, date, location, info);
		participantKeys = new GaeListHolder();
	}

	public Demonstration(String title, Date date, Location location,
			String info, List<String> participants) {
		super(title, date, location, info);
		this.participantKeys = new GaeListHolder(participants);
	}

	public List<String> getParticipantKeys() {
		if(participantKeys == null) {
			participantKeys = new GaeListHolder();
		}
		return participantKeys.getList();
	}

	public void setParticipantKeys(List<String> participants) {
		if(this.participantKeys != null) {
			this.participantKeys.setList(participants);
		} else {
			this.participantKeys = new GaeListHolder(participants);
		}
	}
	
	public List<Participant> getCompetitors() {
		if(participants == null && !getParticipantKeys().isEmpty()) {
			loadParticipants();
		}
		return participants;
	}
	
	public void setCompetitors(List<Participant> participants) {
		this.participants = participants;
	}

	@SuppressWarnings("unchecked")
	private void loadParticipants() {
		PersistenceManager pm = PMF.get().getPersistenceManager();
		Query query = pm.newQuery(Participant.class);
		List<String> keys = getParticipantKeys();
		if (keys != null && !keys.isEmpty()) {
			query.setFilter(":keys.contains(key)");
			participants = (List<Participant>) pm.newQuery(query).execute(keys);
		}
	}
	
}
