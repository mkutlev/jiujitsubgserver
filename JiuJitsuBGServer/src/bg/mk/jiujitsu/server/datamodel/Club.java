package bg.mk.jiujitsu.server.datamodel;

import java.util.List;

import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import javax.jdo.annotations.IdGeneratorStrategy;
import javax.jdo.annotations.IdentityType;
import javax.jdo.annotations.NotPersistent;
import javax.jdo.annotations.PersistenceCapable;
import javax.jdo.annotations.Persistent;
import javax.jdo.annotations.PrimaryKey;

import bg.mk.jiujitsu.server.datastore.PMF;
import bg.mk.jiujitsu.server.utils.KeyUtils;

import com.google.appengine.api.datastore.Key;

@PersistenceCapable(identityType = IdentityType.APPLICATION)
public class Club {

	@PrimaryKey
	@Persistent(valueStrategy = IdGeneratorStrategy.IDENTITY)
	private Key key;

	@Persistent
	private String name;

	@Persistent
	private GaeListHolder trainingHallKeys;

	@NotPersistent
	private List<TrainingHall> trainingHalls;

	@Persistent
	private String info;

	/**
	 * Holder of list of urls pointing to images.
	 */
	@Persistent
	private GaeListHolder imageUrls;

	/**
	 * Holder of list of ids for YouTube videos
	 */
	@Persistent
	private GaeListHolder videoIds;

	/**
	 * This holder keeps keys of coaches as strings
	 */
	@Persistent
	private GaeListHolder coachKeys;

	@Persistent
	private GaeListHolder memberKeys;

	public Club(String name, List<String> trainingHallKeys, String info,
			List<String> imageUrls, List<String> videoIds,
			List<String> coachKeys, List<String> memberKeys) {
		this.name = name;
		this.trainingHallKeys = new GaeListHolder(trainingHallKeys);
		this.info = info;
		this.imageUrls = new GaeListHolder(imageUrls);
		this.videoIds = new GaeListHolder(videoIds);
		this.coachKeys = new GaeListHolder(coachKeys);
		this.memberKeys = new GaeListHolder(memberKeys);
	}

	public Club(String name, String info) {
		this(name, null, info, null, null, null, null);
		this.name = name;
		this.info = info;
	}

	public Key getKey() {
		return key;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getInfo() {
		return info;
	}

	public void setInfo(String info) {
		this.info = info;
	}

	/**
	 * Returns a list of urls for images
	 * 
	 * @return list of strings
	 */
	public List<String> getImageUrls() {
		if (imageUrls == null) {
			imageUrls = new GaeListHolder();
		}
		return imageUrls.getList();
	}

	public void setImageUrls(List<String> imageUrls) {
		if (this.imageUrls != null) {
			this.imageUrls.setList(imageUrls);
		} else {
			this.imageUrls = new GaeListHolder(imageUrls);
		}
	}

	/**
	 * Returns a list of YouTube video ids
	 * 
	 * @return list of ids
	 */
	public List<String> getVideoIds() {
		if (videoIds == null) {
			videoIds = new GaeListHolder();
		}
		return videoIds.getList();
	}

	public void setVideoIds(List<String> videoIds) {
		if (this.videoIds != null) {
			this.videoIds.setList(videoIds);
		} else {
			this.videoIds = new GaeListHolder(videoIds);
		}
	}

	public List<String> getCoachKeys() {
		if (coachKeys == null) {
			coachKeys = new GaeListHolder();
		}
		return coachKeys.getList();
	}

	public void setCoachKeys(List<String> coachKeys) {
		if (this.coachKeys != null) {
			this.coachKeys.setList(coachKeys);
		} else {
			this.coachKeys = new GaeListHolder(coachKeys);
		}
	}

	public List<String> getMemberKeys() {
		if (memberKeys == null) {
			memberKeys = new GaeListHolder();
		}
		return memberKeys.getList();
	}

	public void setMemberKeys(List<String> memberKeys) {
		if (this.memberKeys != null) {
			this.memberKeys.setList(memberKeys);
		} else {
			this.memberKeys = new GaeListHolder(memberKeys);
		}
	}

	public List<String> getTrainingHallKeys() {
		if (trainingHallKeys == null) {
			trainingHallKeys = new GaeListHolder();
		}
		return trainingHallKeys.getList();
	}

	public void setTrainingHallKeys(List<String> trainingHallKeys) {
		if (this.trainingHallKeys != null) {
			this.trainingHallKeys.setList(trainingHallKeys);
		} else {
			this.trainingHallKeys = new GaeListHolder(trainingHallKeys);
		}
	}

	public List<TrainingHall> getTrainingHalls() {
		if (trainingHalls == null && !getTrainingHallKeys().isEmpty()) {
			loadTrainigHalls();
		}
		return trainingHalls;
	}

	public void setTrainingHalls(List<TrainingHall> trainingHalls) {
		this.trainingHalls = trainingHalls;
	}

	@SuppressWarnings("unchecked")
	private void loadTrainigHalls() {
		PersistenceManager pm = PMF.get().getPersistenceManager();
		Query query = pm.newQuery(TrainingHall.class);

		List<String> keys = getTrainingHallKeys();
		if (keys != null && !keys.isEmpty()) {
			query.setFilter(":keys.contains(key)");
			trainingHalls = (List<TrainingHall>) pm.newQuery(query).execute(
					keys);
		}
	}

	/**
	 * Return key represented as string.<br/>
	 * <b>Note:</b> Key is generated when this object is being persisted.
	 * 
	 * @return key as string if key is already created. null otherwise.
	 */
	public String getKeyAsString() {
		return KeyUtils.keyAsString(key);
	}

}
