package bg.mk.jiujitsu.server.datamodel;

import java.util.ArrayList;

public class JiuJitsuData {

	private static JiuJitsuData mInstance;

	private ArrayList<Club> mClubs;
	
	// Private constructor prevents instantiation from other classes
	private JiuJitsuData() {
		mClubs = new ArrayList<Club>();
	}
	
	public static JiuJitsuData getInstance() {
		if (mInstance == null) {
			mInstance = new JiuJitsuData(); 
		}
		
		return mInstance;
	}
	
	public ArrayList<Club> getClubs() {
		return mClubs;
	}

}
