package bg.mk.jiujitsu.server.datamodel;

import java.util.ArrayList;
import java.util.List;

import javax.jdo.annotations.IdGeneratorStrategy;
import javax.jdo.annotations.IdentityType;
import javax.jdo.annotations.NotPersistent;
import javax.jdo.annotations.PersistenceCapable;
import javax.jdo.annotations.Persistent;
import javax.jdo.annotations.PrimaryKey;
import javax.jdo.listener.StoreCallback;

import com.google.appengine.api.datastore.Key;

@PersistenceCapable(identityType = IdentityType.APPLICATION)
public class GaeListHolder implements StoreCallback {

	@PrimaryKey
	@Persistent(valueStrategy = IdGeneratorStrategy.IDENTITY)
	private Key key;
	
	@Persistent
	private String listAsString;
	
	@NotPersistent
	private List<String> list;
	
	@NotPersistent
	private boolean isListCreated = false;
	
	public GaeListHolder() {
		
	}
	
	public GaeListHolder(List<String> list) {
		this.list = list;
	}

	public Key getKey() {
		return key;
	}

	public List<String> getList() {
		if (list == null && !isListCreated) {
			createList();
			isListCreated = true;
		}
		return list;
	}

	public void setList(List<String> list) {
		this.list = list;
	}
	
	public void addString(String s) {
		getList().add(s);
	}

	@Override
	public void jdoPreStore() {
		//Copies list items in listAsString
		if(list != null && !list.isEmpty()) {
			StringBuilder sb = new StringBuilder(32);
			sb.append(list.get(0));
			for(int i=1;i<list.size();i++) {
				sb.append(",").append(list.get(i));
			}
			listAsString = sb.toString();
		} else {
			listAsString = "";
		}
	}
	
	private void createList() {
		list = new ArrayList<String>();
		
		if(listAsString == null || listAsString.isEmpty()) {
			return;
		}
		
		String[] strArray = listAsString.split("[,| ]+");
		for (int i = 0; i < strArray.length; i++) {
			if(!strArray[i].isEmpty()) {
				list.add(strArray[i]);
			}
		}
	}
	
}
