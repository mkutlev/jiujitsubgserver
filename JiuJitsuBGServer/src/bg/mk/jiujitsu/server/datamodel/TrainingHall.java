package bg.mk.jiujitsu.server.datamodel;

import java.util.List;

import javax.jdo.annotations.IdGeneratorStrategy;
import javax.jdo.annotations.IdentityType;
import javax.jdo.annotations.PersistenceCapable;
import javax.jdo.annotations.Persistent;
import javax.jdo.annotations.PrimaryKey;

import com.google.appengine.api.datastore.Key;

@PersistenceCapable(identityType = IdentityType.APPLICATION)
public class TrainingHall {

	@PrimaryKey
	@Persistent(valueStrategy = IdGeneratorStrategy.IDENTITY)
	private Key key;

	@Persistent
	private String name;
	
	@Persistent
	private Location location;
	
	/**
	 * This holder keeps keys of coaches as strings
	 */
	@Persistent
	private GaeListHolder coachKeys;
	
	@Persistent
	private List<String> times;
	
	@Persistent
	private String info;

	public TrainingHall(String name, Location location,
			List<String> coachKeys, List<String> times, String info) {
		this.name = name;
		this.location = location;
		this.coachKeys = new GaeListHolder(coachKeys);
		this.times = times;
		this.info = info;
	}

	public TrainingHall(String name, Location location, String info) {
		this.name = name;
		this.location = location;
		this.info = info;
		this.coachKeys = new GaeListHolder();
	}

	public Key getKey() {
		return key;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Location getLocation() {
		return location;
	}

	public void setLocation(Location location) {
		this.location = location;
	}
	
	public List<String> getCoachKeys() {
		if(coachKeys == null) {
			coachKeys = new GaeListHolder();
		}
		return coachKeys.getList();
	}

	public void setCoachKeys(List<String> coachKeys) {
		if(this.coachKeys != null) {
			this.coachKeys.setList(coachKeys);
		} else {
			this.coachKeys = new GaeListHolder(coachKeys);
		}
	}

	public List<String> getTimes() {
		return times;
	}

	public void setTimes(List<String> times) {
		this.times = times;
	}

	public String getInfo() {
		return info;
	}

	public void setInfo(String info) {
		this.info = info;
	}
	
}
