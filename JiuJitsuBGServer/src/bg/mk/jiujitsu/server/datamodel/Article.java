package bg.mk.jiujitsu.server.datamodel;

import java.util.Date;
import java.util.List;

import javax.jdo.annotations.IdGeneratorStrategy;
import javax.jdo.annotations.IdentityType;
import javax.jdo.annotations.PersistenceCapable;
import javax.jdo.annotations.Persistent;
import javax.jdo.annotations.PrimaryKey;

import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.KeyFactory;
import com.google.appengine.api.datastore.Text;

@PersistenceCapable(identityType = IdentityType.APPLICATION)
public class Article {

	@PrimaryKey
	@Persistent(valueStrategy = IdGeneratorStrategy.IDENTITY)
	private Key key;

	@Persistent
	private String title;

	@Persistent
	private String author;

	@Persistent
	private Date creation;

	@Persistent
	private Text text;

	/**
	 * Holder of list of urls pointing to images.
	 */
	@Persistent
	private GaeListHolder imageUrls;

	/**
	 * Holder of list of ids for YouTube videos
	 */
	@Persistent
	private GaeListHolder videoIds;

	public Article(String title, String author, Date creation, String text) {
		this(title, author, creation, text, null, null);
		this.title = title;
		this.author = author;
		this.creation = creation;
		this.text = new Text(text);
	}

	public Article(String title, String author, Date creation, String text,
			List<String> imageUrls, List<String> videoIds) {
		this.title = title;
		this.author = author;
		this.creation = creation;
		this.text = new Text(text);
		this.imageUrls = new GaeListHolder(imageUrls);
		this.videoIds = new GaeListHolder(videoIds);
	}

	public Key getKey() {
		return key;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getAuthor() {
		return author;
	}

	public void setAuthor(String author) {
		this.author = author;
	}

	public Date getCreation() {
		return creation;
	}

	public void setCreation(Date date) {
		this.creation = date;
	}

	/**
	 * Returns a list of urls for images
	 * 
	 * @return list of strings
	 */
	public List<String> getImageUrls() {
		if (imageUrls == null) {
			imageUrls = new GaeListHolder();
		}
		return imageUrls.getList();
	}

	public void setImageUrls(List<String> imageUrls) {
		if (this.imageUrls != null) {
			this.imageUrls.setList(imageUrls);
		} else {
			this.imageUrls = new GaeListHolder(imageUrls);
		}
	}

	/**
	 * Returns a list of YouTube video ids
	 * 
	 * @return list of ids
	 */
	public List<String> getVideoIds() {
		if (videoIds == null) {
			videoIds = new GaeListHolder();
		}
		return videoIds.getList();
	}

	public void setVideoIds(List<String> videoIds) {
		if (this.videoIds != null) {
			this.videoIds.setList(videoIds);
		} else {
			this.videoIds = new GaeListHolder(videoIds);
		}
	}

	public String getText() {
		if(text == null) {
			return null;
		}
		return text.getValue();
	}

	public void setText(String text) {
		this.text = new Text(text);
	}

	/**
	 * Return key represented as string.<br/>
	 * <b>Note:</b> Key is generated when this object is being persisted.
	 * 
	 * @return key as string if key is already created. null otherwise.
	 */
	public String getKeyAsString() {
		if (key != null) {
			return KeyFactory.keyToString(key);
		}
		return null;
	}

}
