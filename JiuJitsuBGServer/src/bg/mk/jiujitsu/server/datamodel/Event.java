package bg.mk.jiujitsu.server.datamodel;

import java.util.Date;
import java.util.List;

import javax.jdo.annotations.IdGeneratorStrategy;
import javax.jdo.annotations.IdentityType;
import javax.jdo.annotations.Inheritance;
import javax.jdo.annotations.InheritanceStrategy;
import javax.jdo.annotations.PersistenceCapable;
import javax.jdo.annotations.Persistent;
import javax.jdo.annotations.PrimaryKey;
import javax.jdo.listener.DeleteCallback;
import javax.jdo.listener.StoreCallback;

import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.KeyFactory;

@PersistenceCapable(identityType = IdentityType.APPLICATION)
@Inheritance(strategy = InheritanceStrategy.SUBCLASS_TABLE)
public abstract class Event implements DeleteCallback, StoreCallback {

	@PrimaryKey
	@Persistent(valueStrategy = IdGeneratorStrategy.IDENTITY)
	private Key key;

	@Persistent
	private String title;

	@Persistent
	private Date date;

	@Persistent
	private Location location;

	/**
	 * Holder of list of urls pointing to images.
	 */
	@Persistent
	private GaeListHolder imageUrls;
	
	/**
	 * Holder of list of ids for YouTube videos
	 */
	@Persistent
	private GaeListHolder videoIds;

	/**
	 * More info about the event
	 */
	@Persistent
	private String info;

	public Event(String title, Date date, Location location, String info) {
		this(title, date, location, null, null, info);
	}

	public Event(String title, Date date, Location location,
			List<String> imageUrls, List<String> videoIds, String info) {
		this.title = title;
		this.date = date;
		this.location = location;
		this.info = info;
		this.imageUrls = new GaeListHolder(imageUrls);
		this.videoIds = new GaeListHolder(videoIds); 
	}

	public Key getKey() {
		return key;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public Location getLocation() {
		return location;
	}

	public void setLocation(Location location) {
		this.location = location;
	}

	/**
	 * Returns a list of urls for images
	 * 
	 * @return list of strings
	 */
	public List<String> getImageUrls() {
		if (imageUrls == null) {
			imageUrls = new GaeListHolder();
			}
		return imageUrls.getList();
	}

	public void setImageUrls(List<String> imageUrls) {
		if(this.imageUrls != null) {
			this.imageUrls.setList(imageUrls);
		} else {
			this.imageUrls = new GaeListHolder(imageUrls);
		}
	}

	/**
	 * Returns a list of YouTube video ids
	 * 
	 * @return list of ids
	 */
	public List<String> getVideoIds() {
		if (videoIds == null) {
			videoIds = new GaeListHolder();
		}
		return videoIds.getList();
	}

	public void setVideoIds(List<String> videoIds) {
		if(this.videoIds != null) {
			this.videoIds.setList(videoIds);
		} else {
			this.videoIds = new GaeListHolder(videoIds);
		}
	}

	public String getInfo() {
		return info;
	}

	public void setInfo(String info) {
		this.info = info;
	}

	/**
	 * Return key represented as string.<br/>
	 * <b>Note:</b> Key is generated when this object is being persisted.
	 * 
	 * @return key as string if key is already created. null otherwise.
	 */
	public String getKeyAsString() {
		if (key != null) {
			return KeyFactory.keyToString(key);
		}
		return null;
	}
	
	//This method is never called
	@Override
	public void jdoPreDelete() {
		//Removes all images associated with this event
//		if (imageUrls != null && imageUrls.size()>0) {
//			BlobUtils.deleteBlobsByUrl((String[])imageUrls.toArray());
//		}
	}

	@Override
	public void jdoPreStore() {
		
	}
	
	

}
