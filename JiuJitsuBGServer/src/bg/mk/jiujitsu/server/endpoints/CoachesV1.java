package bg.mk.jiujitsu.server.endpoints;

import java.io.IOException;
import java.util.List;

import javax.annotation.Nullable;
import javax.inject.Named;
import javax.jdo.PersistenceManager;
import javax.jdo.Query;

import bg.mk.jiujitsu.server.JiuJitsuConstants;
import bg.mk.jiujitsu.server.datamodel.Coach;
import bg.mk.jiujitsu.server.datastore.PMF;

import com.google.api.server.spi.config.Api;
import com.google.api.server.spi.config.ApiMethod;

/**
 * Defines v1 of a Coach resource as part of the jiujitsubg API, which
 * provides clients the ability to insert and list coaches.
 */
@Api(name = JiuJitsuConstants.APPLICATION_ID, 
	version = "v1", 
	clientIds = { Ids.WEB_CLIENT_ID, Ids.ANDROID_CLIENT_ID, Ids.IOS_CLIENT_ID }, 
	audiences = { Ids.ANDROID_AUDIENCE })
public class CoachesV1 {
	private static final String DEFAULT_LIMIT = "10";

	/**
	 * Provides the ability to query for a collection of Coach entities.
	 * 
	 * @param limit maximum number of entries to return
	 * @param keys filter
	 * @return the collection of Coach entities
	 * @throws IOException
	 */
	@ApiMethod(name = "coaches.list")
	@SuppressWarnings("unchecked")
	public List<Coach> list(@Nullable @Named("limit") String limit,
			@Nullable @Named("keys") List<String> keys) throws IOException {
		PersistenceManager pm = getPersistenceManager();
		Query query = pm.newQuery(Coach.class);
		
		query.setOrdering("degree desc");
		if (limit == null) {
			limit = DEFAULT_LIMIT;
		}
		query.setRange(0, new Long(limit));
		
		List<Coach> coaches;
		//Query only keys in the list
		if (keys != null && !keys.isEmpty()) {
			query.setFilter(":keys.contains(key)");
			coaches = (List<Coach>) pm.newQuery(query).execute(keys);
		} else {
			coaches = (List<Coach>) pm.newQuery(query).execute();
		}
		
		return coaches;
	}
	
	private static PersistenceManager getPersistenceManager() {
		return PMF.get().getPersistenceManager();
	}
	
}
