package bg.mk.jiujitsu.server.endpoints;

import java.io.IOException;
import java.util.List;

import javax.annotation.Nullable;
import javax.inject.Named;
import javax.jdo.PersistenceManager;
import javax.jdo.Query;

import bg.mk.jiujitsu.server.JiuJitsuConstants;
import bg.mk.jiujitsu.server.datamodel.Competition;
import bg.mk.jiujitsu.server.datastore.PMF;

import com.google.api.server.spi.config.Api;
import com.google.api.server.spi.config.ApiMethod;

/**
 * Defines v1 of a Competition resource as part of the jiujitsubg API, which
 * provides clients the ability to list competitions.
 */
@Api(name = JiuJitsuConstants.APPLICATION_ID, 
	version = "v1", 
	clientIds = { Ids.WEB_CLIENT_ID, Ids.ANDROID_CLIENT_ID, Ids.IOS_CLIENT_ID }, 
	audiences = { Ids.ANDROID_AUDIENCE })
public class CompetitionsV1 {
	private static final String DEFAULT_LIMIT = "10";

	/**
	 * Provides the ability to query for a collection of Competition entities.
	 * 
	 * @param limit maximum number of entries to return
	 * @param keys filter by keys
	 * @return the collection of Competition entities
	 * @throws IOException
	 */
	@ApiMethod(name = "competitions.list")
	@SuppressWarnings("unchecked")
	public List<Competition> list(@Nullable @Named("limit") String limit,
			@Nullable @Named("keys") List<String> keys) throws IOException {
		PersistenceManager pm = getPersistenceManager();
		Query query = pm.newQuery(Competition.class);
		
		query.setOrdering("date desc");
		if (limit == null) {
			limit = DEFAULT_LIMIT;
		}
		query.setRange(0, new Long(limit));
		
		List<Competition> competitions;
		//Query only keys in the list
		if (keys != null && !keys.isEmpty()) {
			query.setFilter(":keys.contains(key)");
			competitions = (List<Competition>) pm.newQuery(query).execute(keys);
		} else {
			competitions = (List<Competition>) pm.newQuery(query).execute();
		}
		
		return competitions;
	}
	
	private static PersistenceManager getPersistenceManager() {
		return PMF.get().getPersistenceManager();
	}
	
}
