package bg.mk.jiujitsu.server.endpoints;

import java.io.IOException;
import java.util.List;

import javax.annotation.Nullable;
import javax.inject.Named;
import javax.jdo.PersistenceManager;
import javax.jdo.Query;

import bg.mk.jiujitsu.server.JiuJitsuConstants;
import bg.mk.jiujitsu.server.datamodel.Club;
import bg.mk.jiujitsu.server.datastore.PMF;

import com.google.api.server.spi.config.Api;
import com.google.api.server.spi.config.ApiMethod;

/**
 * Defines v1 of a Club resource as part of the jiujitsubg API, which
 * provides clients the ability to list clubs.
 */
@Api(name = JiuJitsuConstants.APPLICATION_ID, 
	version = "v1", 
	clientIds = { Ids.WEB_CLIENT_ID, Ids.ANDROID_CLIENT_ID, Ids.IOS_CLIENT_ID }, 
	audiences = { Ids.ANDROID_AUDIENCE })
public class ClubsV1 {
	private static final String DEFAULT_LIMIT = "10";

	/**
	 * Provides the ability to query for a collection of Club entities.
	 * 
	 * @param limit maximum number of entries to return
	 * @param order how the entries should be ordered
	 * @param keys filter by keys
	 * @return the collection of Club entities
	 * @throws IOException
	 */
	@ApiMethod(name = "clubs.list")
	@SuppressWarnings("unchecked")
	public List<Club> list(@Nullable @Named("limit") String limit,
			@Nullable @Named("keys") List<String> keys) throws IOException {
		PersistenceManager pm = getPersistenceManager();
		Query query = pm.newQuery(Club.class);
		if (limit == null) {
			limit = DEFAULT_LIMIT;
		}
		query.setRange(0, new Long(limit));
		
		List<Club> clubs;
		//Query only keys in the list
		if (keys != null && !keys.isEmpty()) {
			query.setFilter(":keys.contains(key)");
			clubs = (List<Club>) pm.newQuery(query).execute(keys);
		} else {
			clubs = (List<Club>) pm.newQuery(query).execute();
		}
		
		return clubs;
	}
	
	private static PersistenceManager getPersistenceManager() {
		return PMF.get().getPersistenceManager();
	}
	
}
