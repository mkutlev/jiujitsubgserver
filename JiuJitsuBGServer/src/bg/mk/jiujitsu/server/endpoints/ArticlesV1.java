package bg.mk.jiujitsu.server.endpoints;

import java.io.IOException;
import java.util.List;

import javax.annotation.Nullable;
import javax.inject.Named;
import javax.jdo.PersistenceManager;
import javax.jdo.Query;

import bg.mk.jiujitsu.server.JiuJitsuConstants;
import bg.mk.jiujitsu.server.datamodel.Article;
import bg.mk.jiujitsu.server.datastore.PMF;

import com.google.api.server.spi.config.Api;
import com.google.api.server.spi.config.ApiMethod;

/**
 * Defines v1 of an Article resource as part of the jiujitsubg API, which
 * provides clients the ability to list articles.
 */
@Api(name = JiuJitsuConstants.APPLICATION_ID, 
	version = "v1", 
	clientIds = { Ids.WEB_CLIENT_ID, Ids.ANDROID_CLIENT_ID, Ids.IOS_CLIENT_ID }, 
	audiences = { Ids.ANDROID_AUDIENCE })
public class ArticlesV1 {
	private static final String DEFAULT_LIMIT = "30";

	/**
	 * Provides the ability to query for a collection of Article entities.
	 * 
	 * @param limit maximum number of entries to return
	 * @param keys only articles with key in this list will be listed.
	 * It this parameter is null, all articles will be listed.
	 * @return the collection of Competition entities
	 * @throws IOException
	 */
	@ApiMethod(name = "articles.list")
	@SuppressWarnings("unchecked")
	public List<Article> list(@Nullable @Named("limit") String limit,
			@Nullable @Named("keys") List<String> keys) throws IOException {
		PersistenceManager pm = getPersistenceManager();
		Query query = pm.newQuery(Article.class);
		
		query.setOrdering("creation desc");
		if (limit == null) {
			limit = DEFAULT_LIMIT;
		}
		query.setRange(0, new Long(limit));
		
		List<Article> articles;
		//Query only keys in the list
		if (keys != null && !keys.isEmpty()) {
			query.setFilter(":keys.contains(key)");
			articles = (List<Article>) pm.newQuery(query).execute(keys);
		} else {
			articles = (List<Article>) pm.newQuery(query).execute();
		}
		
		return articles;
	}
	
	private static PersistenceManager getPersistenceManager() {
		return PMF.get().getPersistenceManager();
	}
	
}
