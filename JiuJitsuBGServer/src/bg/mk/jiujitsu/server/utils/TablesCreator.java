package bg.mk.jiujitsu.server.utils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.jdo.PersistenceManager;

import bg.mk.jiujitsu.server.datamodel.Article;
import bg.mk.jiujitsu.server.datamodel.Competition;
import bg.mk.jiujitsu.server.datamodel.Competitor;
import bg.mk.jiujitsu.server.datamodel.Judge;
import bg.mk.jiujitsu.server.datamodel.Location;
import bg.mk.jiujitsu.server.datastore.PMF;

public class TablesCreator {

	private TablesCreator() {
	}
	
	public static void createTables() {
		PersistenceManager pm = PMF.get().getPersistenceManager();
		
//		createCompetition(pm);
		createArticle(pm);
	}
	
	private static void createCompetition(PersistenceManager pm) {
		Competitor competitor1 = new Competitor("Стоян", "Петков", 26, 77, -78, "Златен медал в дисциплина Файтинг");
		pm.makePersistent(competitor1);
		Competitor competitor2 = new Competitor("Живко", "Сираков", 26, 72, -78, "Златен медал в дисциплина Файтинг");
		pm.makePersistent(competitor2);
		
		Judge judge1 = new Judge("Мартин", "Матеев", 29);
		pm.makePersistent(judge1);
		Judge judge2 = new Judge("Ивайло", "Т", 28);
		pm.makePersistent(judge2);
		
		String dateStr = "23.06.2013 13:30";
		SimpleDateFormat sdf = new SimpleDateFormat("dd.MM.yyyy HH:mm");
		Date date = null;
		try {
			date = sdf.parse(dateStr);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		
		Location location = new Location("България", "София", "Овергаз Арена Младост");
		List<String> imageUrls = new ArrayList<String>();
		imageUrls.add("url1"); 
		imageUrls.add("url2");
		List<String> videoIds = new ArrayList<String>();
		videoIds.add("url1"); 
		videoIds.add("url2");
		Competition competition = new Competition("Републиканско по джу-джицу", date, location, imageUrls, videoIds, "info");
		
		List<String> judgeKeys = new ArrayList<String>();
		judgeKeys.add(judge1.getKeyAsString());
		judgeKeys.add(judge2.getKeyAsString());
		competition.setJudgeKeys(judgeKeys);
		
		List<String> competitorKeys = new ArrayList<String>();
		competitorKeys.add(competitor1.getKeyAsString());
		competitorKeys.add(competitor2.getKeyAsString());
		competition.setCompetitorKeys(competitorKeys);
		pm.makePersistent(competition);
	}
	
	private static void createArticle(PersistenceManager pm) {
		pm.makePersistent(new Article("Create Table", "Author", new Date(), "text"));
	}
}
