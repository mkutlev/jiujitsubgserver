package bg.mk.jiujitsu.server.utils;

import java.util.ArrayList;
import java.util.List;

import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.KeyFactory;

public class KeyUtils {

	// Suppress default constructor for noninstantiability
	private KeyUtils() {
		throw new AssertionError();
	}

	/**
	 * Return key represented as string.<br/>
	 * <b>Note:</b> Key is generated when this object is being persisted.
	 * 
	 * @return key as string if key is already created. null otherwise.
	 */
	public static String keyAsString(Key key) throws IllegalArgumentException {
		if (key != null) {
			return KeyFactory.keyToString(key);
		}
		return null;
	}

	public static Key stringToKey(String keyAsString)
			throws IllegalArgumentException {
		if (keyAsString != null) {
			return KeyFactory.stringToKey(keyAsString);
		}
		return null;
	}

	/**
	 * Splits keys
	 * 
	 * @param keysString
	 *            list of keys represented as a string
	 * @return list of split keys
	 */
	public static List<String> getKeys(String keysString) {
		String[] keysArr = keysString.split("[,| ]+");
		List<String> keys = new ArrayList<String>();
		for (int i = 0; i < keysArr.length; i++) {
			if (!keysArr[i].isEmpty()) {
				keys.add(keysArr[i]);
			}
		}
		return keys;
	}

}
