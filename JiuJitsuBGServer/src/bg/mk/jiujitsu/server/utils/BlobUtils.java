package bg.mk.jiujitsu.server.utils;

import bg.mk.jiujitsu.server.JiuJitsuConstants;

import com.google.appengine.api.blobstore.BlobKey;
import com.google.appengine.api.blobstore.BlobstoreFailureException;
import com.google.appengine.api.blobstore.BlobstoreService;
import com.google.appengine.api.blobstore.BlobstoreServiceFactory;

public class BlobUtils {

	// Suppress default constructor for noninstantiability
	private BlobUtils() {
		throw new AssertionError();
	}

	public static String getBlobUrl(String blobKey) {
		if (blobKey != null && !blobKey.isEmpty()) {
			return JiuJitsuConstants.SERVER_HOST + "/resource?key=" + blobKey;
		}
		return null;
	}

	public static String getBlobKey(String blobUrl) {
		if (blobUrl != null && !blobUrl.isEmpty()) {
			return null;
		}
		int start = blobUrl.indexOf("key=");
		if (start != -1) {
			start += "key=".length();
		} else {
			// key parameter does not exist in this url
			return null;
		}

		if (start >= blobUrl.length()) {
			// the url ends after "key=" and there is no value
			return null;
		}
		int nextParameterStart = blobUrl.indexOf('&', start);
		int end;
		if (nextParameterStart != -1) {
			end = nextParameterStart;
		} else {
			end = blobUrl.length();
		}

		if (start == end - 1) {
			// the value of key is empty string
			return null;
		}
		return blobUrl.substring(start, end);
	}

	public static void deleteBlobs(BlobKey... blobKeys)
			throws BlobstoreFailureException {
		BlobstoreService blobstoreService = BlobstoreServiceFactory
				.getBlobstoreService();
		blobstoreService.delete(blobKeys);
	}

	public static void deleteBlobs(String... blobKeys)
			throws BlobstoreFailureException {
		BlobKey[] keys = new BlobKey[blobKeys.length];
		for (int i = 0; i < blobKeys.length; i++) {
			keys[i] = new BlobKey(blobKeys[i]);
		}
		deleteBlobs(keys);
	}

	public static void deleteBlobsByUrl(String... blobUrls)
			throws BlobstoreFailureException {
		String[] blobKeys = new String[blobUrls.length];
		for (int i = 0; i < blobUrls.length; i++) {
			blobKeys[i] = getBlobKey(blobUrls[i]);
		}
		deleteBlobs(blobKeys);
	}

}
