package bg.mk.jiujitsu.server.admin.servlets;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.appengine.api.blobstore.BlobstoreService;
import com.google.appengine.api.blobstore.BlobstoreServiceFactory;
import com.google.appengine.api.users.User;
import com.google.appengine.api.users.UserService;
import com.google.appengine.api.users.UserServiceFactory;

public class Upload extends HttpServlet {

	private BlobstoreService blobstoreService = BlobstoreServiceFactory
			.getBlobstoreService();

	@Override
	public void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws IOException, ServletException {

		UserService userService = UserServiceFactory.getUserService();
		User user = userService.getCurrentUser();

		String uploadURL = blobstoreService
				.createUploadUrl("/admin/admin_main/mediastore/post");

		req.setAttribute("uploadURL", uploadURL);
		req.setAttribute("user", user);

		RequestDispatcher dispatcher = req
				.getRequestDispatcher("/WEB-INF/templates/upload.jsp");
		dispatcher.forward(req, resp);
	}
	
}
