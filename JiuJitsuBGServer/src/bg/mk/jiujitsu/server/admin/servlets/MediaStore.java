package bg.mk.jiujitsu.server.admin.servlets;

import java.io.IOException;
import java.util.List;

import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import bg.mk.jiujitsu.server.datamodel.MediaObject;
import bg.mk.jiujitsu.server.datastore.PMF;

import com.google.appengine.api.users.User;
import com.google.appengine.api.users.UserService;
import com.google.appengine.api.users.UserServiceFactory;

public class MediaStore extends HttpServlet {

	@Override
	public void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws IOException, ServletException {

		UserService userService = UserServiceFactory.getUserService();
		User user = userService.getCurrentUser();

		PersistenceManager pm = PMF.get().getPersistenceManager();

		Query query = pm.newQuery(MediaObject.class);
		query.setOrdering("creation desc");
		//TODO Do not show all files in one page
		// query.setRange(fromIncl, toExcl)

		List<MediaObject> results = (List<MediaObject>) query.execute();

		String[] errors = req.getParameterValues("error");
		if (errors == null)
			errors = new String[0];

		req.setAttribute("errors", errors);
		req.setAttribute("files", results);
		req.setAttribute("user", user);
		RequestDispatcher dispatcher = req
				.getRequestDispatcher("/WEB-INF/templates/media_store.jsp");
		dispatcher.forward(req, resp);
	}
	
}
