package bg.mk.jiujitsu.server.admin.servlets;

import java.io.IOException;
import java.util.List;

import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import bg.mk.jiujitsu.server.datamodel.Competition;
import bg.mk.jiujitsu.server.datamodel.Demonstration;
import bg.mk.jiujitsu.server.datamodel.Seminar;
import bg.mk.jiujitsu.server.datastore.PMF;

import com.google.appengine.api.users.User;
import com.google.appengine.api.users.UserService;
import com.google.appengine.api.users.UserServiceFactory;

public class Events extends HttpServlet {

	@Override
	public void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws IOException, ServletException {

		UserService userService = UserServiceFactory.getUserService();
		User user = userService.getCurrentUser();

		PersistenceManager pm = PMF.get().getPersistenceManager();

		Query competitionsQuery = pm.newQuery(Competition.class);
		competitionsQuery.setOrdering("date desc");
		List<Competition> competitions = (List<Competition>) competitionsQuery.execute();

		Query seminarsQuery = pm.newQuery(Seminar.class);
		seminarsQuery.setOrdering("date desc");
		List<Seminar> seminars = (List<Seminar>) seminarsQuery.execute();

		Query demonstrationsQuery = pm.newQuery(Demonstration.class);
		demonstrationsQuery.setOrdering("date desc");
		List<Competition> demonstrations = (List<Competition>) demonstrationsQuery.execute();

		
		String[] errors = req.getParameterValues("error");
		if (errors == null)
			errors = new String[0];

		req.setAttribute("errors", errors);
		req.setAttribute("user", user);
		req.setAttribute("competitions", competitions);
		req.setAttribute("seminars", seminars);
		req.setAttribute("demonstrations", demonstrations);
		
		RequestDispatcher dispatcher = req
				.getRequestDispatcher("/WEB-INF/templates/events.jsp");
		dispatcher.forward(req, resp);
	}
	
}
