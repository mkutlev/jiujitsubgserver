package bg.mk.jiujitsu.server.admin.servlets;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.jdo.PersistenceManager;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import bg.mk.jiujitsu.server.datamodel.Coach;
import bg.mk.jiujitsu.server.datastore.PMF;

public class AddCoachPost extends HttpServlet {

	private static final String DATE_PATTERN = "MM.yyyy";

	@Override
	public void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws IOException {
		String firstName = req.getParameter("fName");
		String lastName = req.getParameter("lName");
		String startCoachingDateStr = req.getParameter("startCoachingDate");
		SimpleDateFormat dateFormat = new SimpleDateFormat(DATE_PATTERN);
		Date startCoachingDate= null;
		try {
			startCoachingDate = dateFormat.parse(startCoachingDateStr);
		} catch (ParseException ex) {
			System.err.println(ex.getMessage());
		}
		
		int age = 0;
		try {
			age = Integer.parseInt(req.getParameter("age"));
		} catch (NumberFormatException ex) {
			// This is not valid int value
		}
		int degree = 0;
		try {
			degree = Integer.parseInt(req.getParameter("degree"));
		} catch (NumberFormatException ex) {
			// This is not valid int value
		}
		String info = req.getParameter("info");
		
		List<String> imageUrls = new ArrayList<String>();
		int imageIndex = 0;
		String imageUrl = req.getParameter("img" + imageIndex);
		while (imageUrl != null) {
			if(!imageUrl.isEmpty()) {
				imageUrls.add(imageUrl);
			}
			imageIndex++;
			imageUrl = req.getParameter("img" + imageIndex);
		}

		List<String> videoIds = new ArrayList<String>();
		int videoIdIndex = 0;
		String videoId = req.getParameter("videoid" + videoIdIndex);
		
		while (videoId != null) {
			if(!videoId.isEmpty()) {
				videoIds.add(videoId);
			}
			videoIdIndex++;
			videoId = req.getParameter("videoid" + videoIdIndex);
		}

		Coach coach;
		if (!imageUrls.isEmpty() || !videoIds.isEmpty()) {
			coach = new Coach(firstName, lastName, startCoachingDate, degree, info, imageUrls, videoIds);
		} else {
			coach = new Coach(firstName, lastName, startCoachingDate, degree, info);
		}
		coach.setAge(age);
		
		PersistenceManager pm = PMF.get().getPersistenceManager();
		pm.makePersistent(coach);
		resp.sendRedirect("/admin/admin_main/coaches");
	}
	
}
