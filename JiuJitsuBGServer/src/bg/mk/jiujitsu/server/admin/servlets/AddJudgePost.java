package bg.mk.jiujitsu.server.admin.servlets;

import java.io.IOException;

import javax.jdo.PersistenceManager;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import bg.mk.jiujitsu.server.datamodel.Judge;
import bg.mk.jiujitsu.server.datastore.PMF;

public class AddJudgePost extends HttpServlet {
	
	@Override
	public void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws IOException {
		String firstName = req.getParameter("fName");
		String lastName = req.getParameter("lName");
		
		int age = 0;
		try {
			age = Integer.parseInt(req.getParameter("age"));
		} catch (NumberFormatException ex) {
			// This is not valid int value
		}
		
		Judge judge = new Judge(firstName, lastName, age);
		
		PersistenceManager pm = PMF.get().getPersistenceManager();
		pm.makePersistent(judge);
		resp.sendRedirect("/admin/admin_main/judges");
	}
	
}
