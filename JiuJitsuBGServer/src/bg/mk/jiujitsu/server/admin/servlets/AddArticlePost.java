package bg.mk.jiujitsu.server.admin.servlets;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.jdo.PersistenceManager;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import bg.mk.jiujitsu.server.datamodel.Article;
import bg.mk.jiujitsu.server.datastore.PMF;

public class AddArticlePost extends HttpServlet {

	private static final String DATE_PATTERN = "dd.MM.yyyy";

	@Override
	public void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws IOException {
		String title = req.getParameter("title");
		String author = req.getParameter("author");
		String text = req.getParameter("text");
		String dateStr = req.getParameter("creation");
		SimpleDateFormat dateFormat = new SimpleDateFormat(DATE_PATTERN);
		Date dateCreated = null;
		try {
			dateCreated = dateFormat.parse(dateStr);
		} catch (ParseException e) {
			e.printStackTrace();
		}

		List<String> imageUrls = new ArrayList<String>();
		int imageIndex = 0;
		String imageUrl = req.getParameter("img" + imageIndex);
		while (imageUrl != null) {
			if (!imageUrl.isEmpty()) {
				imageUrls.add(imageUrl);
			}
			imageIndex++;
			imageUrl = req.getParameter("img" + imageIndex);
		}

		List<String> videoIds = new ArrayList<String>();
		int videoIdIndex = 0;
		String videoId = req.getParameter("videoid" + videoIdIndex);

		while (videoId != null) {
			if (!videoId.isEmpty()) {
				videoIds.add(videoId);
			}
			videoIdIndex++;
			videoId = req.getParameter("videoid" + videoIdIndex);
		}

		Article article;
		if (!imageUrls.isEmpty() || !videoIds.isEmpty()) {
			article = new Article(title, author, dateCreated, text, imageUrls,
					videoIds);
		} else {
			article = new Article(title, author, dateCreated, text);
		}

		PersistenceManager pm = PMF.get().getPersistenceManager();
		pm.makePersistent(article);
		resp.sendRedirect("/admin/admin_main/articles");
	}

}
