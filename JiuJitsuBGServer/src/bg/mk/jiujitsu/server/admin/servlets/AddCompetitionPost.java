package bg.mk.jiujitsu.server.admin.servlets;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.jdo.PersistenceManager;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import bg.mk.jiujitsu.server.datamodel.Competition;
import bg.mk.jiujitsu.server.datamodel.Location;
import bg.mk.jiujitsu.server.datastore.PMF;

public class AddCompetitionPost extends HttpServlet {

	private static final String DATE_PATTERN = "dd.MM.yyyy HH:mm";

	@Override
	public void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws IOException {
		String title = req.getParameter("title");
		String dateStr = req.getParameter("date");
		SimpleDateFormat dateFormat = new SimpleDateFormat(DATE_PATTERN);
		Date date = null;
		try {
			date = dateFormat.parse(dateStr);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		Location location = new Location(req.getParameter("country"),
				req.getParameter("city"), req.getParameter("place"));
		String info = req.getParameter("info");

		List<String> imageUrls = new ArrayList<String>();
		int imageIndex = 0;
		String imageUrl = req.getParameter("img" + imageIndex);
		while (imageUrl != null) {
			if (!imageUrl.isEmpty()) {
				imageUrls.add(imageUrl);
			}
			imageIndex++;
			imageUrl = req.getParameter("img" + imageIndex);
		}

		List<String> videoIds = new ArrayList<String>();
		int videoIdIndex = 0;
		String videoId = req.getParameter("videoid" + videoIdIndex);

		while (videoId != null) {
			if (!videoId.isEmpty()) {
				videoIds.add(videoId);
			}
			videoIdIndex++;
			videoId = req.getParameter("videoid" + videoIdIndex);
		}

		Competition competition;
		if (!imageUrls.isEmpty() || !videoIds.isEmpty()) {
			competition = new Competition(title, date, location, imageUrls,
					videoIds, info);
		} else {
			competition = new Competition(title, date, location, info);
		}

		PersistenceManager pm = PMF.get().getPersistenceManager();
		pm.makePersistent(competition);
		resp.sendRedirect("/admin/admin_main/events");
	}

}
