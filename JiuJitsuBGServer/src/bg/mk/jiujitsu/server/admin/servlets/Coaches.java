package bg.mk.jiujitsu.server.admin.servlets;

import java.io.IOException;
import java.util.List;

import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import bg.mk.jiujitsu.server.datamodel.Coach;
import bg.mk.jiujitsu.server.datastore.PMF;

import com.google.appengine.api.users.User;
import com.google.appengine.api.users.UserService;
import com.google.appengine.api.users.UserServiceFactory;

public class Coaches extends HttpServlet {

	@Override
	public void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws IOException, ServletException {

		UserService userService = UserServiceFactory.getUserService();
		User user = userService.getCurrentUser();

		PersistenceManager pm = PMF.get().getPersistenceManager();
		Query coachesQuery = pm.newQuery(Coach.class);
		coachesQuery.setOrdering("startCoachingDate desc");
		List<Coach> coaches = (List<Coach>) coachesQuery.execute();
		
		String[] errors = req.getParameterValues("error");
		if (errors == null)
			errors = new String[0];

		req.setAttribute("errors", errors);
		req.setAttribute("user", user);
		req.setAttribute("coaches", coaches);
		
		RequestDispatcher dispatcher = req
				.getRequestDispatcher("/WEB-INF/templates/coaches.jsp");
		dispatcher.forward(req, resp);
	}
	
}
