package bg.mk.jiujitsu.server.admin.servlets;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.jdo.PersistenceManager;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import bg.mk.jiujitsu.server.datamodel.Club;
import bg.mk.jiujitsu.server.datastore.PMF;
import bg.mk.jiujitsu.server.utils.KeyUtils;

public class AddClubPost extends HttpServlet {

	@Override
	public void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws IOException {
		String name = req.getParameter("name");
		String info = req.getParameter("info");
		String coachKeys = req.getParameter("coaches");
		String trainingHallKeys = req.getParameter("trainingHalls");
		
		List<String> imageUrls = new ArrayList<String>();
		int imageIndex = 0;
		String imageUrl = req.getParameter("img" + imageIndex);
		while (imageUrl != null) {
			if(!imageUrl.isEmpty()) {
				imageUrls.add(imageUrl);
			}
			imageIndex++;
			imageUrl = req.getParameter("img" + imageIndex);
		}

		List<String> videoIds = new ArrayList<String>();
		int videoIdIndex = 0;
		String videoId = req.getParameter("videoid" + videoIdIndex);
		
		while (videoId != null) {
			if(!videoId.isEmpty()) {
				videoIds.add(videoId);
			}
			videoIdIndex++;
			videoId = req.getParameter("videoid" + videoIdIndex);
		}

		Club club = new Club(name, info);
		club.setImageUrls(imageUrls);
		club.setVideoIds(videoIds);
		club.setCoachKeys(KeyUtils.getKeys(coachKeys));
		club.setTrainingHallKeys(KeyUtils.getKeys(trainingHallKeys));
		
		PersistenceManager pm = PMF.get().getPersistenceManager();
		pm.makePersistent(club);
		resp.sendRedirect("/admin/admin_main/clubs");
	}
	
}
