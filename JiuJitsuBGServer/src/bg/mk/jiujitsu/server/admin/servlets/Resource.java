package bg.mk.jiujitsu.server.admin.servlets;

import java.io.IOException;
import java.net.URLEncoder;
import java.util.List;

import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import bg.mk.jiujitsu.server.datamodel.MediaObject;
import bg.mk.jiujitsu.server.datastore.PMF;

import com.google.appengine.api.blobstore.BlobKey;
import com.google.appengine.api.blobstore.BlobstoreService;
import com.google.appengine.api.blobstore.BlobstoreServiceFactory;
import com.google.appengine.api.images.Image;
import com.google.appengine.api.images.ImagesService;
import com.google.appengine.api.images.ImagesServiceFactory;
import com.google.appengine.api.images.Transform;

@SuppressWarnings("serial")
public class Resource extends HttpServlet {
	private BlobstoreService blobstoreService = BlobstoreServiceFactory
			.getBlobstoreService();

	@Override
	public void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws IOException {

		BlobKey blobKey = new BlobKey(req.getParameter("key"));

		PersistenceManager pm = PMF.get().getPersistenceManager();

		Query query = pm.newQuery(MediaObject.class, "blob == blobParam");
		query.declareImports("import "
				+ "com.google.appengine.api.blobstore.BlobKey");
		query.declareParameters("BlobKey blobParam");

		@SuppressWarnings("unchecked")
		List<MediaObject> results = (List<MediaObject>) query.execute(blobKey);
		if (results.isEmpty()) {
			resp.sendRedirect("/?error="
					+ URLEncoder.encode("BlobKey does not exist", "UTF-8"));
			return;
		}

		MediaObject result = results.get(0);

		// Here the image can be transformed depending on the resolution of the
		// device
		String rotation = req.getParameter("rotate");
		if (rotation != null && !"".equals(rotation)
				&& !"null".equals(rotation)) {
			int degrees = Integer.parseInt(rotation);
			ImagesService imagesService = ImagesServiceFactory
					.getImagesService();
			Image image = ImagesServiceFactory.makeImageFromBlob(blobKey);
			Transform rotate = ImagesServiceFactory.makeRotate(degrees);
			Image newImage = imagesService.applyTransform(rotate, image);
			byte[] imgbyte = newImage.getImageData();

			resp.setContentType(result.getContentType());
			resp.getOutputStream().write(imgbyte);
			return;
		}
		blobstoreService.serve(blobKey, resp);
	}
}
