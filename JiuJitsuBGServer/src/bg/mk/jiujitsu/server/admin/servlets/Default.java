package bg.mk.jiujitsu.server.admin.servlets;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.appengine.api.users.User;
import com.google.appengine.api.users.UserService;
import com.google.appengine.api.users.UserServiceFactory;

public class Default extends HttpServlet {

	@Override
	public void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws IOException, ServletException {

//		UserService userService = UserServiceFactory.getUserService();
//		User user = userService.getCurrentUser();
//
//		String uploadURL = "/admin/admin_main/judges/addjudgepost";
//
//		req.setAttribute("uploadURL", uploadURL);
//		req.setAttribute("user", user);
//
		//TODO
		RequestDispatcher dispatcher = req
				.getRequestDispatcher("/WEB-INF/templates/style.css");
		dispatcher.forward(req, resp);
	}

}
