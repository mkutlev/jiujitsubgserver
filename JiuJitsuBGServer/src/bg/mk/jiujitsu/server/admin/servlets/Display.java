package bg.mk.jiujitsu.server.admin.servlets;

import java.io.IOException;
import java.net.URLEncoder;
import java.util.List;

import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import bg.mk.jiujitsu.server.datamodel.MediaObject;
import bg.mk.jiujitsu.server.datastore.PMF;

import com.google.appengine.api.blobstore.BlobKey;
import com.google.appengine.api.users.User;
import com.google.appengine.api.users.UserService;
import com.google.appengine.api.users.UserServiceFactory;

public class Display extends HttpServlet {

	@Override
	public void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws IOException, ServletException {

		String blobKeyString = req.getParameter("key");
		if (blobKeyString == null || blobKeyString.equals("")) {
			resp.sendRedirect("/?error="
					+ URLEncoder.encode("BlobKey not provided", "UTF-8"));
			return;
		}

		BlobKey blobKey = new BlobKey(blobKeyString);
		PersistenceManager pm = PMF.get().getPersistenceManager();

		Query query = pm.newQuery(MediaObject.class, "blob == blobParam");
		query.declareImports("import "
				+ "com.google.appengine.api.blobstore.BlobKey");
		query.declareParameters("BlobKey blobParam");

		@SuppressWarnings("unchecked")
		List<MediaObject> results = (List<MediaObject>) query.execute(blobKey);
		if (results.isEmpty()) {
			resp.sendRedirect("/?error="
					+ URLEncoder.encode("BlobKey does not exist", "UTF-8"));
			return;
		}

		UserService userService = UserServiceFactory.getUserService();
		User user = userService.getCurrentUser();

		MediaObject result = results.get(0);

		String rotation = req.getParameter("rotate");
		String displayURL = result.getURLPath() + "&rotate=" + rotation;

		req.setAttribute("displayURL", displayURL);
		req.setAttribute("user", user);
		req.setAttribute("rotation", rotation);
		req.setAttribute("item", result);
		req.setAttribute("blobkey", blobKeyString);

		RequestDispatcher dispatcher = req
				.getRequestDispatcher("/WEB-INF/templates/display.jsp");
		dispatcher.forward(req, resp);
	}
}
