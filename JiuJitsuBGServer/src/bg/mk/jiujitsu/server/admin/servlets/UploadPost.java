package bg.mk.jiujitsu.server.admin.servlets;

import java.io.IOException;
import java.net.URLEncoder;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import bg.mk.jiujitsu.server.datamodel.MediaObject;
import bg.mk.jiujitsu.server.datastore.PMF;

import com.google.appengine.api.blobstore.BlobInfo;
import com.google.appengine.api.blobstore.BlobInfoFactory;
import com.google.appengine.api.blobstore.BlobKey;
import com.google.appengine.api.blobstore.BlobstoreService;
import com.google.appengine.api.blobstore.BlobstoreServiceFactory;
import com.google.appengine.api.users.User;
import com.google.appengine.api.users.UserService;
import com.google.appengine.api.users.UserServiceFactory;

public class UploadPost extends HttpServlet {

	private BlobstoreService blobstoreService = BlobstoreServiceFactory
			.getBlobstoreService();

	@Override
	public void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws IOException {

		UserService userService = UserServiceFactory.getUserService();
		User user = userService.getCurrentUser();

		Map<String, List<BlobKey>> blobs = blobstoreService.getUploads(req);

		if (blobs.keySet().isEmpty()) {
			resp.sendRedirect("/?error="
					+ URLEncoder.encode("No file uploaded", "UTF-8"));
			return;
		}

		Iterator<String> names = blobs.keySet().iterator();
		String blobName = names.next();
		BlobKey blobKey = blobs.get(blobName).get(0);

		BlobInfoFactory blobInfoFactory = new BlobInfoFactory();
		BlobInfo blobInfo = blobInfoFactory.loadBlobInfo(blobKey);

		String contentType = blobInfo.getContentType();
		long size = blobInfo.getSize();
		Date creation = blobInfo.getCreation();
		String fileName = blobInfo.getFilename();

		String title = req.getParameter("title");
		String description = req.getParameter("description");
		boolean isShared = "public".equalsIgnoreCase(req.getParameter("share"));

		try {
			MediaObject mediaObj = new MediaObject(user, blobKey, creation,
					contentType, fileName, size, title, description, isShared);
			PMF.get().getPersistenceManager().makePersistent(mediaObj);
			resp.sendRedirect("/admin/admin_main/mediastore");
		} catch (Exception e) {
			blobstoreService.delete(blobKey);
			resp.sendRedirect("/?error="
					+ URLEncoder.encode(
							"Object save failed: " + e.getMessage(), "UTF-8"));
		}
	}
	
}
