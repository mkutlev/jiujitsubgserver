package bg.mk.jiujitsu.server;

public class JiuJitsuConstants {
	
	private JiuJitsuConstants() {
	}

	public static final String SERVER_HOST = "jiujitsubg.appspot.com";
	public static final String APPLICATION_ID = "jiujitsubg";
	public static final String PROJECT_ID = "jiu-jitsubg";
	public static final String PROJECT_NUMBER = "1092055684824";

}
