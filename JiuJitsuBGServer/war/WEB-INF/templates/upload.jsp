<%@ page language="java" pageEncoding="utf-8" contentType="text/html;charset=utf-8"%>
<%@ page import="com.google.appengine.api.users.User" %>
<%
  User user = (User) request.getAttribute("user");
  String uploadURL = (String) request.getAttribute("uploadURL");
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01//EN" 
  "http://www.w3.org/TR/html4/strict.dtd">

<html lang="en">
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
  <title>MediaStore Upload</title>
</head>
<body>
  <div align="right">
    Здравей, <%= user.getNickname() %>!
  </div>

  <form action="<%= uploadURL %>" method="POST" enctype="multipart/form-data">
    Споделяне: <select name="share">
     	  <option value="public">Public</option>
          <option value="private">Private</option>
        </select>
    Заглавие: <input type="text" size="40" name="title"><br>
    Описание:<br>
    <textarea cols="80" rows="20" name="description"></textarea><br>
    Качи файла: <input type="file" name="file"><br>
    <input type="submit" name="submit" value="Запази">
  </form>

  <hr>
  <a href="/admin/admin_main/mediastore">Изход</a>
</body>
</html>