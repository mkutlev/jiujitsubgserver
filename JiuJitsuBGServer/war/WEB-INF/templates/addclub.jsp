<%@page language="java" pageEncoding="utf-8" contentType="text/html;charset=utf-8"%>
<%@page import="com.google.appengine.api.users.User" %>
<%
  User user = (User) request.getAttribute("user");
  String uploadURL = (String) request.getAttribute("uploadURL");
  int imagesCount = 5;
  int videosCount = 5;
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01//EN" 
  "http://www.w3.org/TR/html4/strict.dtd">

<html lang="en">
<head>
<script>
	function validateForm() {
		if (document.myForm.name.value == "") {
			alert("Name should not be blank");
			document.myForm.name.focus();
			return false;
		} else if (document.myForm.info.value == "") {
			alert("Info should not be blank");
			document.myForm.info.focus();
			return false;
		} 
	}
</script>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
  <title>Coach Upload</title>
</head>
<body>
  <div align="right">
    Здравей, <%= user.getNickname() %>!
  </div>

  <form name="myForm" action="<%= uploadURL %>" method="POST" onSubmit="return validateForm()">
    Име: <input type="text" size="40" name="name"><br/>
    Информация:<br/>
    <textarea cols="80" rows="20" name="info"></textarea><br>
    Треньори (Ключове):<br>
    <textarea cols="80" rows="5" name="coaches"></textarea><br>
    Зали за тренировки (Ключове):<br>
    <textarea cols="80" rows="5" name="trainingHalls"></textarea><br>
    Линкове към снимки:<br/>
    <%
    	for(int i=0;i<imagesCount;i++) { %>
    		<input type="text" size="100" name="img<%= i %>"><br/>	
    <% 	} %>
    Id-та за YouTube видеа:<br/>
		<% for (int i = 0; i < videosCount; i++) { %>
		<input type="text" size="100" name="videoid<%= i %>"><br/>	
    <% 	} %>
    <input type="submit" name="submit" value="Запази">
  </form>

  <hr>
  <a href="/admin/admin_main/clubs">Изход</a>
</body>
</html>

