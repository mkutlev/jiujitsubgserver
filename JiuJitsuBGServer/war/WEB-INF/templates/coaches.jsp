<%@ page language="java" pageEncoding="utf-8" contentType="text/html;charset=utf-8" %>
<%@ page import="com.google.appengine.api.users.User" %>
<%@ page import="bg.mk.jiujitsu.server.datamodel.Coach" %>
<%@ page import="java.util.List" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%
  User user = (User) request.getAttribute("user");
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Coaches</title>
</head>
<body>
<div align="right">
      <%
        if (user != null) {
      %>
        <b><i>Здравей, <%= user.getNickname() %>!</i></b>
      <% } %>
    </div>

    <ul>
        <% 
           String[] errors = (String[]) request.getAttribute("errors");
           for (int i = 0; i < errors.length; i++) { %>
          <li style="color: red"><%= errors[i]%></li>
        <% } %>
      </ul>

    <h1 align="center">
    	Треньори
    </h1>
    <hr>
	<%
		SimpleDateFormat dateFormat = new SimpleDateFormat("MM.yyyy");
		List<Coach> coaches = (List<Coach>) request.getAttribute("coaches");
		if (coaches.size() > 0) {
			for (int i = 0; i < coaches.size(); i++) {
				Coach coach = coaches.get(i);
	%>
	<b>
               <a href="/admin/admin_main/articles/display_coach">
               <%= coach.getFirstName() %> <%= coach.getLastName() %></a></b> - Възраст: <%= coach.getAge() %>
               <% if(coach.getStartCoachingDate() != null) { %>
               Треньор от <%= dateFormat.format(coach.getStartCoachingDate()) %>,
               <% } %>
               Степен: <%= coach.getDegree() %><br/>
               Key: <%= coach.getKeyAsString() %><br/>
      <%
            }
         } else {
      %>
        <div align="center">
            Няма въведени треньори!
        </div>
      <% } %>

      <% if (user != null) { %>
        <hr>
        <a href="/admin/admin_main/coaches/addcoach">Добавете треньор</a>
      <% } %>
      <hr>
      
      <a href="/admin/admin_main">Назад</a>
</body>
</html>