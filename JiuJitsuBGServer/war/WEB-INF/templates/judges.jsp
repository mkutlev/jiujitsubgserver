<%@ page language="java" pageEncoding="utf-8" contentType="text/html;charset=utf-8" %>
<%@ page import="com.google.appengine.api.users.User" %>
<%@ page import="bg.mk.jiujitsu.server.datamodel.Judge" %>
<%@ page import="java.util.List" %>
<%
  User user = (User) request.getAttribute("user");
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Judges</title>
</head>
<body>
<div align="right">
      <%
        if (user != null) {
      %>
        <b><i>Здравей, <%= user.getNickname() %>!</i></b>
      <% } %>
    </div>

    <ul>
        <% 
           String[] errors = (String[]) request.getAttribute("errors");
           for (int i = 0; i < errors.length; i++) { %>
          <li style="color: red"><%= errors[i]%></li>
        <% } %>
      </ul>

    <h1 align="center">
    	Съдии
    </h1>
    <hr>
	<%
		List<Judge> judges = (List<Judge>) request.getAttribute("judges");
		if (judges.size() > 0) {
			for (int i = 0; i < judges.size(); i++) {
				Judge judge = judges.get(i);
	%>
               <b><%= judge.getFirstName() %> <%= judge.getLastName() %></b> - Възраст: <%= judge.getAge() %>
               Key: <%= judge.getKeyAsString() %><br/>
      <%
            }
         } else {
      %>
        <div align="center">
            Няма въведени съдии!
        </div>
      <% } %>

      <% if (user != null) { %>
        <hr>
        <a href="/admin/admin_main/judges/addjudge">Добавете съдия</a>
      <% } %>
      <hr>
      
      <a href="/admin/admin_main">Назад</a>
</body>
</html>