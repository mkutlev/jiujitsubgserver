<%@ page language="java" pageEncoding="utf-8" contentType="text/html;charset=utf-8" %>
<%@ page import="com.google.appengine.api.users.User" %>
<%@ page import="bg.mk.jiujitsu.server.datamodel.Club" %>
<%@ page import="java.util.List" %>
<%
  User user = (User) request.getAttribute("user");
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Clubs</title>
</head>
<body>
<div align="right">
      <%
        if (user != null) {
      %>
        <b><i>Здравей, <%= user.getNickname() %>!</i></b>
      <% } %>
    </div>

    <ul>
        <% 
           String[] errors = (String[]) request.getAttribute("errors");
           for (int i = 0; i < errors.length; i++) { %>
          <li style="color: red"><%= errors[i]%></li>
        <% } %>
      </ul>

    <h1 align="center">
    	Джу-джицу клубове
    </h1>
    <hr>
	<%
		List<Club> clubs = (List<Club>) request.getAttribute("clubs");
		if (clubs.size() > 0) {
			for (int i = 0; i < clubs.size(); i++) {
				Club club = clubs.get(i);
	%>
	<b>
               <a href="/admin/admin_main/clubs/display_club">
               <%= club.getName() %></a></b><br/>
               Информация: <%= club.getInfo() %><br/>
               Key: <%= club.getKeyAsString() %><br/>
      <%
            }
         } else {
      %>
        <div align="center">
            <b><i>Няма въведени клубове!</i></b>
        </div>
      <% } %>

      <% if (user != null) { %>
        <hr>
        <a href="/admin/admin_main/clubs/addclub">Добавете клуб</a>
      <% } %>
      <hr>
      
      <a href="/admin/admin_main">Назад</a>
</body>
</html>