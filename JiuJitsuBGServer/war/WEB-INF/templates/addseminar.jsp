<%@page language="java" pageEncoding="utf-8" contentType="text/html;charset=utf-8"%>
<%@page import="com.google.appengine.api.users.User" %>
<%
  User user = (User) request.getAttribute("user");
  String uploadURL = (String) request.getAttribute("uploadURL");
  int imagesCount = 5;
  int videosCount = 5;
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01//EN" 
  "http://www.w3.org/TR/html4/strict.dtd">

<html lang="en">
<head>
<script>
	function validateForm() {
		if (document.myForm.title.value == "") {
			alert("Title should not be left blank");
			document.myForm.title.focus();
			return false;
		} else if ((document.myForm.date.value == "")
				|| (document.myForm.date.value == "dd.MM.yyyy HH:mm")) {
			alert("Add appropriate date");
			document.myForm.date.focus();
			return false;
		} else if (document.myForm.country.value == "") {
			alert("Add country");
			document.myForm.country.focus();
			return false;
		} else if (document.myForm.city.value == "") {
			alert("Add city");
			document.myForm.city.focus();
			return false;
		} else if (document.myForm.place.value == "") {
			alert("Add place");
			document.myForm.place.focus();
			return false;
		}
	}
</script>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
  <title>Seminar Upload</title>
</head>
<body>
  <div align="right">
    Здравей, <%= user.getNickname() %>!
  </div>

  <form name="myForm" action="<%= uploadURL %>" method="POST" onSubmit="return validateForm()">
    Заглавие: <input type="text" size="40" name="title"><br/>
    Дата и час: <input type="text" size="20" name="date" value="dd.MM.yyyy HH:mm"><br/>
    Местоположение: <br/> 
    Държава: <input type="text" size="40" name="country"><br/>
    Град: <input type="text" size="40" name="city"><br/>
    По точно местоположение: <input type="text" size="80" name="place"><br/>
    Треньори (Ключове):<br> 
    <textarea cols="80" rows="5" name="coaches"></textarea><br>
    Допълнителна Информация:<br>
    <textarea cols="80" rows="20" name="info"></textarea><br>
    Линкове към снимки:<br/>
    <%
    	for(int i=0;i<imagesCount;i++) { %>
    		<input type="text" size="100" name="img<%= i %>"><br/>	
    <% 	} %>
    Id-та за YouTube видеа:<br/>
		<% for (int i = 0; i < videosCount; i++) { %>
		<input type="text" size="100" name="videoid<%= i %>"><br/>	
    <% 	} %>
    <input type="submit" name="submit" value="Запази">
  </form>

  <hr>
  <a href="/admin/admin_main/events">Изход</a>
</body>
</html>

