<%@ page language="java" pageEncoding="utf-8" contentType="text/html;charset=utf-8"%>
<%@ page import="com.google.appengine.api.users.User"%>
<%@ page import="bg.mk.jiujitsu.server.datamodel.Article"%>
<%@ page import="java.util.List"%>
<%@ page import="java.text.SimpleDateFormat"%>
<%
  User user = (User) request.getAttribute("user");
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Articles</title>
</head>
<body>
	<div align="right">
		<%
        if (user != null) {
      %>
		<b><i>Здравей, <%= user.getNickname() %>!
		</i></b>
		<% } %>
	</div>

	<ul>
		<% 
           String[] errors = (String[]) request.getAttribute("errors");
           for (int i = 0; i < errors.length; i++) { %>
		<li style="color: red"><%= errors[i]%></li>
		<% } %>
	</ul>

	<h1 align="center">Статии</h1>
	<hr>
	<%
      	 SimpleDateFormat dateFormat = new SimpleDateFormat("dd.MM.yyyy");
         List<Article> articles =
           (List<Article>) request.getAttribute("articles");
         if (articles.size() > 0) {
           for (int i = 0; i < articles.size(); i++) {
             Article article = articles.get(i);
      %>
	<b> <%-- <a href="<%=comp.getDisplayURL()%>"> --%> <a
		href="/admin/admin_main/articles/display_article"> "<%=article.getTitle()%>"
	</a></b>: От <%= article.getAuthor() %>, създадена:
	<%= dateFormat.format(article.getCreation()) %><br /> 
	Key: <%= article.getKeyAsString() %><br />
	<%
           }
         } else {
      %>
	<div align="center">Няма намерени статии!</div>
	<% } %>

	<% if (user != null) { %>
	<hr>
	<a href="/admin/admin_main/articles/addarticle">Добавете нова статия</a>
	<% } %>
	<hr>

	<a href="/admin/admin_main">Назад</a>
</body>
</html>