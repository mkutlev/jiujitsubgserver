<%@ page language="java" pageEncoding="utf-8" contentType="text/html;charset=utf-8"%>
<%@ page import="com.google.appengine.api.users.User" %>
<%@ page import="java.util.List" %>
<%
  User user = (User) request.getAttribute("user");
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01//EN"
  "http://www.w3.org/TR/html4/strict.dtd">

<html lang="en">
<!-- <link rel="stylesheet" type="text/css" href="/style.css" /> -->
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <title>JiuJitsu Admin</title>
  </head>
  <body>
    <div align="right">
      <%
        if (user != null) {
      %>
        <b><i>Здравей, <%= user.getNickname() %>!
		</i></b>
      <% } %>

    </div>

    <ul>
        <% 
           String[] errors = (String[]) request.getAttribute("errors");
           for (int i = 0; i < errors.length; i++) { %>
          <li style="color: red"><%= errors[i]%></li>
        <% } %>
      </ul>

    <h1 align="center">
        Джу-джицу База данни
    </h1>
    
    <div id="content">
    
    <div id="content_top"></div>
    
	<div id="content_main">
	
    <hr>
    
    	<h2>&nbsp;&nbsp;<a href="/admin/admin_main/articles">Статии и новини</a></h2>
   	
   	
    	<h2>&nbsp;&nbsp;<a href="/admin/admin_main/events">Събития - състезания, семинари, демонстрации</a></h2>
  
  	
    	<h2>&nbsp;&nbsp;<a href="/admin/admin_main/clubs">Джу-джицу клубове</a></h2>
    

    	<h2>&nbsp;&nbsp;<a href="/admin/admin_main/coaches">Треньори</a></h2>
    
   
    	<h2>&nbsp;&nbsp;<a href="/admin/admin_main/judges">Съдии</a></h2>
  
    	
    	<h2>&nbsp;&nbsp;<a href="/admin/admin_main/mediastore">Медиа - снимки, картинки, др</a></h2>
    
    <hr>
    </div>
    
    <div id="content_bottom"></div>
    
    </div>
  </body>
</html>
