<%@page language="java" pageEncoding="utf-8" contentType="text/html;charset=utf-8"%>
<%@page import="com.google.appengine.api.users.User" %>
<%
  User user = (User) request.getAttribute("user");
  String uploadURL = (String) request.getAttribute("uploadURL");
  int imagesCount = 5;
  int videosCount = 5;
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01//EN" 
  "http://www.w3.org/TR/html4/strict.dtd">

<html lang="en">
<head>
<script>
	function validateForm() {
		if (document.myForm.title.value == "") {
			alert("Title should not be left blank");
			document.myForm.title.focus();
			return false;
		} else if ((document.myForm.creation.value == "")
				|| (document.myForm.creation.value == "dd.MM.yyyy")) {
			alert("Add appropriate date");
			document.myForm.creation.focus();
			return false;
		} else if (document.myForm.text.value == "") {
			alert("Add text of the article");
			document.myForm.text.focus();
			return false;
		} 
	}
</script>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
  <title>Article Upload</title>
</head>
<body>
  <div align="right" style="border-color: aqua;">
  
    Здравей, <%= user.getNickname() %>!
  </div>

  <form name="myForm" action="<%= uploadURL %>" method="POST" onSubmit="return validateForm()">
    Заглавие: <input type="text" size="40" name="title"><br/>
    Автор (име и фамилия): <input type="text" size="40" name="author"><br/>
    Дата на създаване: <input type="text" size="20" name="creation" value="dd.MM.yyyy"><i>Example: 21.07.2013</i><br/>
    Текст (<big>HTML</big>):<br>
    <textarea cols="80" rows="30" name="text"></textarea><br>
    Линкове към снимки:<br/>
    <%
    	for(int i=0;i<imagesCount;i++) { %>
    		<input type="text" size="100" name="img<%= i %>"><br/>	
    <% 	} %>
    Id-та за YouTube видеа:<br/>
		<% for (int i = 0; i < videosCount; i++) { %>
		<input type="text" size="100" name="videoid<%= i %>"><br/>	
    <% 	} %>
    <input type="submit" name="submit" value="Запази">
  </form>

  <hr>
  <a href="/admin/admin_main/articles">Изход</a>
</body>
</html>

