<%@ page language="java" pageEncoding="utf-8" contentType="text/html;charset=utf-8"%>
<%@ page import="com.google.appengine.api.users.User" %>
<%@ page import="bg.mk.jiujitsu.server.datamodel.Competition" %>
<%@ page import="bg.mk.jiujitsu.server.datamodel.Demonstration" %>
<%@ page import="bg.mk.jiujitsu.server.datamodel.Seminar" %>
<%@ page import="java.util.List" %>
<%
  User user = (User) request.getAttribute("user");
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Events</title>
</head>
<body>
<div align="right">
      <%
        if (user != null) {
      %>
        Здравей, <%= user.getNickname() %>!
      <% } %>
    </div>

    <ul>
        <% 
           String[] errors = (String[]) request.getAttribute("errors");
           for (int i = 0; i < errors.length; i++) { %>
          <li style="color: red"><%= errors[i]%></li>
        <% } %>
      </ul>

    <h1 align="center">
    	Събития
    </h1>
    <hr>
    
    <!-- Lists all competitions -->
    <h2 align="center">Състезания</h2>
      <%
         List<Competition> competitions =
           (List<Competition>) request.getAttribute("competitions");
         if (competitions.size() > 0) {
           for (int i = 0; i < competitions.size(); i++) {
             Competition comp = competitions.get(i);
      %>
            <b>
            	<%-- <a href="<%=comp.getDisplayURL()%>"> --%>
                <a href="/admin/admin_main/events/display_competition">
               "<%=comp.getTitle()%>"
            </a></b>:
            <%=comp.getDate()%> <%=comp.getLocation().getCity()%>, <%=comp.getLocation().getCountry()%><br/>
            Key: <%=comp.getKeyAsString() %><br/>
      <%
           }
         } else {
      %>
        <div align="center">
            Няма намерени състезания!
        </div>
      <% } %>

      <% if (user != null) { %>
        <hr>
        <a href="/admin/admin_main/events/addcompetition">Добавете новo състезание</a>
      <% } %>
      <hr>
      
       <!-- Lists all seminars -->
          <h2 align="center">Семинари</h2>
      <%
         List<Seminar> seminars =
           (List<Seminar>) request.getAttribute("seminars");
         if (seminars.size() > 0) {
           for (int i = 0; i < seminars.size(); i++) {
        	   Seminar seminar = seminars.get(i);
      %>
            <b>
           	    <%-- <a href="<%=seminar.getDisplayURL()%>"> --%>
                <a href="/admin/admin_main/events/display_seminar">
               "<%=seminar.getTitle()%>"
            </a></b>:
            <%=seminar.getDate()%> <%=seminar.getLocation().getCity()%>, <%=seminar.getLocation().getCountry()%><br/>
            Key: <%=seminar.getKeyAsString() %><br/>
            <%
            	if(seminars.get(i).getCoachKeys() != null && !seminars.get(i).getCoachKeys().isEmpty()) {
            %> Coaches: <%
            	for (int j=0;j<seminars.get(i).getCoachKeys().size();j++) {
            %>
	               <%=seminars.get(i).getCoachKeys().get(j)%>, 
	            <%	} %> <br/> 
           <%  } 
          }
         } else {
      %>
        <div align="center">
            Няма намерени семинари!
        </div>
      <% } %>

      <% if (user != null) { %>
        <hr>
        <a href="/admin/admin_main/events/addseminar">Добавете нов семинар</a>
      <% } %>
      <hr>
      
      <!-- Lists all demonstrations -->
          <h2 align="center">Демонстрации</h2>
      <%
         List<Demonstration> demonstrations =
           (List<Demonstration>) request.getAttribute("demonstrations");
         if (demonstrations.size() > 0) {
           for (int i = 0; i < demonstrations.size(); i++) {
        	   Demonstration demonstr = demonstrations.get(i);
      %>
            <b>
            	<%-- <a href="<%=demonstr.getDisplayURL()%>"> --%>
                <a href="/admin/admin_main/events/display_demonstration">
               "<%=demonstr.getTitle()%>"
            </a></b>:
            <%=demonstr.getDate()%> <%=demonstr.getLocation().getCity()%>, <%=demonstr.getLocation().getCountry()%><br/>
            Key: <%=demonstr.getKeyAsString() %><br/>
      <%
           }
         } else {
      %>
        <div align="center">
            Няма намерени демонстрации!
        </div>
      <% } %>

      <% if (user != null) { %>
        <hr>
        <a href="/admin/admin_main/events/adddemonstration">Добавете нова демонстрация</a>
      <% } %>
      <hr>
      
      <a href="/admin/admin_main">Назад</a>
</body>
</html>