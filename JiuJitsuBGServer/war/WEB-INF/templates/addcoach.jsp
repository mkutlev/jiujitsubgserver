<%@page language="java" pageEncoding="utf-8" contentType="text/html;charset=utf-8"%>
<%@page import="com.google.appengine.api.users.User" %>
<%
  User user = (User) request.getAttribute("user");
  String uploadURL = (String) request.getAttribute("uploadURL");
  int imagesCount = 5;
  int videosCount = 5;
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01//EN" 
  "http://www.w3.org/TR/html4/strict.dtd">

<html lang="en">
<head>
<script>
	function validateForm() {
		if (document.myForm.fName.value == "") {
			alert("First name should not be blank");
			document.myForm.fName.focus();
			return false;
		} else if (document.myForm.lName.value == "") {
			alert("Last name should not be blank");
			document.myForm.lName.focus();
			return false;
		} 
	}
</script>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
  <title>Coach Upload</title>
</head>
<body>
  <div align="right">
    Здравей, <%= user.getNickname() %>!
  </div>

  <form name="myForm" action="<%= uploadURL %>" method="POST" onSubmit="return validateForm()">
    Име: <input type="text" size="40" name="fName"><br/>
    Фамилия: <input type="text" size="40" name="lName"><br/>
    Възраст: <input type="text" size="40" name="age"><br/>
    Треньор от: <input type="text" size="20" name="startCoachingDate" value="MM.yyyy"><i>Example: 07.1990</i><br/>
	Степен: <select name="degree">
     	  <option value="11">Първи Дан</option>
          <option value="12">Втори Дан</option>
          <option value="13">Трети Дан</option>
          <option value="14">Четвърти Дан</option>
          <option value="15">Пети Дан</option>
          <option value="16">Шести Дан</option>
          <option value="17">Седми Дан</option>
          <option value="18">Осми Дан</option>
          <option value="19">Девети Дан</option>
          <option value="1">Първо Кю</option>
        </select><br/>
    Допълнителна Информация:<br/>
    <textarea cols="80" rows="20" name="info"></textarea><br>
    Линкове към снимки:<br/>
    <%
    	for(int i=0;i<imagesCount;i++) { %>
    		<input type="text" size="100" name="img<%= i %>"><br/>	
    <% 	} %>
    Id-та за YouTube видеа:<br/>
		<% for (int i = 0; i < videosCount; i++) { %>
		<input type="text" size="100" name="videoid<%= i %>"><br/>	
    <% 	} %>
    <input type="submit" name="submit" value="Запази">
  </form>

  <hr>
  <a href="/admin/admin_main/coaches">Изход</a>
</body>
</html>

