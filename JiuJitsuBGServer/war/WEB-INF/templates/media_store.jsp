<%@ page language="java" pageEncoding="utf-8" contentType="text/html;charset=utf-8"%>
<%@ page import="com.google.appengine.api.users.User" %>
<%@ page import="bg.mk.jiujitsu.server.datamodel.MediaObject" %>
<%@ page import="java.util.List" %>
<%
  User user = (User) request.getAttribute("user");
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01//EN"
  "http://www.w3.org/TR/html4/strict.dtd">

<html lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <title>MediaStore</title>
  </head>
  <body>
    <div align="right">
      <%
        if (user != null) {
      %>
        Здравей, <%= user.getNickname() %>!
      <% } %>
    </div>

    <ul>
        <% 
           String[] errors = (String[]) request.getAttribute("errors");
           for (int i = 0; i < errors.length; i++) { %>
          <li style="color: red"><%= errors[i]%></li>
        <% } %>
      </ul>

    <h1 align="center">
    	Медия Файлове
    </h1>

    <hr>
      <%
         List<MediaObject> files =
           (List<MediaObject>) request.getAttribute("files");
         if (files.size() > 0) {
           for (int i = 0; i < files.size(); i++) {
             MediaObject item = files.get(i);
      %>
            <b>
              <% if (item.isImage()) { %>
                <a href="<%=item.getDisplayURL()%>">
              <% } else { %>
                <a href="<%=item.getURLPath()%>">
              <% } %>
               "<%=item.getTitle()%>"
            </a></b>:
            <%=item.getSize()%> <%=item.getCreationTime()%>
            <%=item.getContentType()%><br>
            <%=item.getDescription()%><br>
            <%=item.getFullURLPath()%><br>
      <%
           }
         } else {
      %>
        <div align="center">
            Няма намерени медия файлове!
        </div>
      <% } %>

      <% if (user != null) { %>
        <hr>
        <a href="/admin/admin_main/mediastore/upload">Добавете нов файл</a>
      <% } %>
      <hr>
      <a href="/admin/admin_main">Назад</a>
  </body>
</html>
