<%@page language="java" pageEncoding="utf-8" contentType="text/html;charset=utf-8"%>
<%@page import="com.google.appengine.api.users.User" %>
<%
  User user = (User) request.getAttribute("user");
  String uploadURL = (String) request.getAttribute("uploadURL");
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01//EN" 
  "http://www.w3.org/TR/html4/strict.dtd">

<html lang="en">
<head>
<script>
	function validateForm() {
		if (document.myForm.fName.value == "") {
			alert("First name should not be blank");
			document.myForm.fName.focus();
			return false;
		} else if (document.myForm.lName.value == "") {
			alert("Last name should not be blank");
			document.myForm.lName.focus();
			return false;
		} 
	}
</script>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
  <title>Judge Upload</title>
</head>
<body>
  <div align="right">
    <b><i>Здравей, <%= user.getNickname() %>!</i></b>
  </div>

  <form name="myForm" action="<%= uploadURL %>" method="POST" onSubmit="return validateForm()">
    Име: <input type="text" size="40" name="fName"><br/>
    Фамилия: <input type="text" size="40" name="lName"><br/>
    Възраст: <input type="text" size="40" name="age"><br/>
    <input type="submit" name="submit" value="Запази">
  </form>

  <hr>
  <a href="/admin/admin_main/judges">Изход</a>
</body>
</html>

